<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Nalog
 *
 * @author M_Fayziev
 */
class Nalog {
    public static function addNalog($region,$period,$nalog_type,$summa)
    {
        $db = Db::getConnection();
        $sql = "INSERT INTO esh_nalog(id_region,period,nalog_type,summa) "
                . "VALUES(:id_region,:period,:nalog_type,:summa)";
        $result = $db->prepare($sql);
        $result->bindParam(':id_region', $region, PDO::PARAM_INT);
        $result->bindParam(':period', $period, PDO::PARAM_STR);
        $result->bindParam(':nalog_type', $nalog_type, PDO::PARAM_INT);
        $result->bindParam(':summa', $summa, PDO::PARAM_STR);
        $result->execute();
        return $db->lastInsertId();
    }
    public static function editNalog($id,$summa){
        $db = Db::getConnection();
        $sql = "UPDATE esh_nalog SET summa = :summa WHERE id = :id LIMIT 1";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':summa', $summa, PDO::PARAM_STR);
        $result->execute();
        return true;
    }
    public static function plusToNalog($id,$summa){
        $db = Db::getConnection();
        $sql = "UPDATE esh_nalog SET summa = summa + :summa WHERE id = :id LIMIT 1";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':summa', $summa, PDO::PARAM_STR);
        $result->execute();
        return true;
    }
    public static function plusToNaloglist($id_nalog,$list)
    {
        echo $id_nalog;
        $oldList = self::getNalogList($id_nalog);
        $db = Db::getConnection();
        if(is_array($list))
        {
            foreach($oldList as $k=>$v){ #Обновляем существующие записи
               $sql = "UPDATE esh_nalog_list SET summa = summa + ".$list[$v['id_field']].""
                        . " WHERE id = ".$v['id'];
               $db->query($sql);
               unset($list[$v['id_field']]);
            }
            if(count($list)>0){ # Добавляем новые записи
                self::addNalogList($id_nalog, $list);
            }
        }
    }
    public static function addNalogList($id_nalog,$list)
    {
        $db = Db::getConnection();
        $sql = "INSERT INTO esh_nalog_list(id_nalog,id_field,summa) VALUES";
        if(is_array($list))
        {
            foreach($list as $k=>$v){
                $v = self::toDouble($v);
                if($v>0){
                    $sql .= "(".$id_nalog.",".$k.",".$v."),";
                }
            }
            $sql = substr($sql,0,-1);
            $db->query($sql);
        }
    } 
    public static function editNalogList($id_nalog,$list)
    {
        $oldList = self::getNalogList($id_nalog);
        $db = Db::getConnection();
        if(is_array($list))
        {
            foreach($oldList as $k=>$v){ #Обновляем существующие записи
               $sql = "UPDATE esh_nalog_list SET summa = ".$list[$v['id_field']].""
                        . " WHERE id = ".$v['id'];
               $db->query($sql);
               unset($list[$v['id_field']]);
            }
            if(count($list)>0){ # Добавляем новые записи
                self::addNalogList($id_nalog, $list);
            }
        }
    }
    public static function getNalogList($id_nalog = 0,$period = '', $nalog_type = 0)
    {
        $db = Db::getConnection();
        if($id_nalog>0){
            $sql = "SELECT * FROM esh_nalog_list WHERE id_nalog = :id";
        }else{
            $sql = "SELECT l.id_field,SUM(l.summa) summa
                    FROM esh_nalog_list l
                    LEFT JOIN esh_nalog n ON l.id_nalog=n.id
                    WHERE n.nalog_type=".$nalog_type.
                    " AND n.period='".$period."'
                    GROUP BY l.id_field";
        }
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id_nalog, PDO::PARAM_INT);
        $result->execute();
        $res = array();
        while($r = $result->fetch(PDO::FETCH_ASSOC)){
            $res[$r['id_field']] = $r['summa'];
        }
        return $res;
    }
    
    public static function toDouble($str)
    {
        $str = str_replace(',', '.', $str);
        return doubleval($str);
    }
    public static function getRegion($str){
        $db = Db::getConnection();
        $sql = 'SELECT id,name FROM esh_nalog_region WHERE ';
        if(strlen($str)<12){
            $sql .= ' rma = :str LIMIT 1';
        }else{
            $sql .= ' rcb = :str LIMIT 1';
        }
        $result = $db->prepare($sql);
        $result->bindParam(':str', $str, PDO::PARAM_STR);
        $result->execute();
        return $result->fetch(PDO::FETCH_ASSOC);
    }
    
    public static function checkAccessToRegion($uid,$rid)
    {
        return true;
    }
    #Проверить существует ли запись для данного периода для данного региона
    public static function checkPeriod($period,$region,$nalog_type)
    {
        $db = Db::getConnection();
        $sql = 'SELECT id FROM esh_nalog '
                . 'WHERE period = :period '
                . 'AND id_region = :region '
                . 'AND nalog_type = :nalog_type LIMIT 1';
        
        
        $result = $db->prepare($sql);
        $result->bindParam(':period', $period, PDO::PARAM_STR);
        $result->bindParam(':region', $region, PDO::PARAM_INT);
        $result->bindParam(':nalog_type', $nalog_type, PDO::PARAM_INT);
        $result->execute();
        return $result->fetch(PDO::FETCH_ASSOC);
    }
    #При добавлении/изменении сохраняем введенные пользователем данные в лог в сериализованном виде
    public static function addToLog($id_nalog,$id_user,$date,$change_type,$fields)
    {
        $db = Db::getConnection();
        $sql = 'INSERT INTO esh_nalog_log(id_nalog,id_user,date,change_type,fields) '
                . 'VALUES(:id_nalog,:id_user,:date,:change_type,:fields)';        
        $result = $db->prepare($sql);
        $result->bindParam(':id_nalog', $id_nalog, PDO::PARAM_INT);
        $result->bindParam(':id_user', $id_user, PDO::PARAM_INT);
        $result->bindParam(':date', $date, PDO::PARAM_STR);
        $result->bindParam(':change_type', $change_type, PDO::PARAM_INT);
        $result->bindParam(':fields', $fields, PDO::PARAM_LOB);
        $result->execute();
        return $result->fetch(PDO::FETCH_ASSOC);
    }
    #необходимо реализовать показ сохраненной таблицы(вид изм, дата, пользователь)
    public static function getSummFromArr($arr)
    {
        $summa = 0;
        foreach($arr as $k=>$v){
            $summa += intval($v);
        }
        return $summa;
    }
    public static function getRegionArr()
    {
        $db = Db::getConnection();
        $sql = 'SELECT * FROM esh_nalog_region';
        $arr = array();
        $res = $db->query($sql);
        while($r = $res->fetch(PDO::FETCH_ASSOC)){
            $arr[$r['id']] = $r['name'];
        }
        return $arr;
    }
    public static function getMonitoring($period,$region)
    {   
        $nalog_type = Form::getFromSpr(2);
        $db = Db::getConnection();
        
        
        $sql = "SELECT * FROM esh_nalog_region";
        if($region>0){
            $sql .= " WHERE id = ".$region;
        }
        $regions = array();
        $q = $db->query($sql);
        while($r = $q->Fetch()){
            $regions[$r['id']] = $r['name'];
        }
        
        
        $sql = "SELECT * FROM esh_nalog WHERE period = '".$period."'";  
        if($region>0){
            $sql .= " and id_region = ".$region;
        }
        $list = array();
        $q = $db->query($sql);
        while($r = $q->Fetch()){
            $list[$r['id_region']][$r['nalog_type']] = $r['summa'];
        }
        
        
        $tbl = '<table class="GeneratedTable" style="margin-top:30px" align="center"><tr><th>Минтакаи нозироти андоз/намуди эъломия</th>';
        foreach($nalog_type as $k=>$v){
            $tbl .= '<th>'.$v.'</th>';
        }
        $tbl .= '</tr>';
        
        foreach($regions as $k=>$v){
            $tbl .= '<tr><td>'.$v.'</td>';
            foreach($nalog_type as $kk=>$vv){
                $val = (array_key_exists($k,$list) && array_key_exists($kk,$list[$k])  && doubleval($list[$k][$kk])>0 ? 1 : 0);
                $red = ($val<1 ? 'style="background-color:red"' : '');
                $tbl .= '<td '.$red.'>'.$val.'</td>';
            }
            $tbl .= '</tr>';
        }
        $tbl .= '</table>';
        
                  
        return $tbl;    
    }
    
}