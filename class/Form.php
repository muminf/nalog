<?php

/**
 * Description of Form
 *
 * @author M_Fayziev
 */
class Form {
    public static function getRows($idForm)
    {
        $DB = Db::getConnection();
        $sql = "SELECT f.*
                FROM `esh_nalog_form` f 
                WHERE f.`id_form`=".intval($idForm)."
                ORDER BY f.`id_row`,f.id";
       $result = $DB->query($sql); 
       $rows = array();
       while($row = $result->fetch(PDO::FETCH_ASSOC)){
           $rows[$row['id']] = $row;
       }
       return $rows;
    }
    public static function getRowsJSON($idForm)
    {
        $DB = Db::getConnection();
        $sql = "SELECT f.id_row,f.columns 
                FROM esh_nalog_form_new f 
                WHERE f.`id_form`=".intval($idForm)."
                AND f.columns is not null    
                ORDER BY f.`id_row`,f.id";
       #$DB->exec("set names cp1251");
       $result = $DB->query($sql); 
       $rows = array();
       while($row = $result->fetch(PDO::FETCH_ASSOC)){
           $columns = unserialize($row['columns']);
           $rows[$row['id_row']] = $columns;
       }
       return $rows;
    }
     public static function getRowsArrJSON($rows)
     {
        $result = array();
        $maxColspan = self::getMaxColspan($rows);
        foreach($rows as $k=>$v){
            if(is_array($v)){
                foreach($v as $kk=>$vv){
                    $vv["colspan"] = $maxColspan - count($v);
                    $result[$k][$kk] = $vv;;
                }
                $vv["colspan"] = $maxColspan - count($v) + 1;
                $result[$k][$kk] = $vv;
            }
            #$firstCol = false;
            #$v["colspan"] = $maxColspan - count($v) + 1;
            #$result[$k][$kk] = $vv;
        }
        return $result;
    }
    public static function getRowsArr($rows)
    {
        
        $rowsArr = array();
        $result = array();
        foreach($rows as $k=>$v){
            $rowsArr[$v['id_row']][] = $v;
        }
        $maxColspan = self::getMaxColspan($rowsArr);
        foreach ($rowsArr as $k=>$v){
             foreach($v as $kk=>$vv){
                 $vv["colspan"] = $maxColspan - count($v);
                 $result[$k][$kk] = $vv;
                 $firstCol = false;
             }
              $vv["colspan"] = $maxColspan - count($v) + 1;
              $result[$k][$kk] = $vv;
        }
        return $result;
    }
    
    public static function getMaxColspan($rowsArr)
    {
        $result = 0;
        foreach($rowsArr as $arr){
            $result = (count($arr)>$result ? count($arr) : $result);
        }
        return $result;
    }
    public static function printTable($idForm,$params,$fields = null,$show = false)
    {   
        $rowsJSON = self::getRowsJSON($idForm);
        $rowsArr = self::getRowsArrJSON($rowsJSON);
     
        $paramsStr = '';
        if(is_array($params) && count($params)>0){
            foreach($params as $k=>$v){
                $paramsStr .=  ' '.$k.'="'.$v.'" ';
            }
        }
        $tbl = '<form class="appnitro" enctype="multipart/form-data" method="post" action="php/handler.php">'
                . '<table '.$paramsStr.' >';
        foreach($rowsArr as $k=>$v){
            $tbl .= '<tr>';
            foreach($v as $kk=>$vv){
                $center = ($vv['colspan']>3 ? 'align="center" style="font-weight:600"' : "");
                $colspan = ($vv['colspan']>1 ? 'colspan="'.$vv['colspan'].'"' : "");
                $rowspan = '';
                $colspan = (isset($vv['cs']) && intval($vv['cs'])>0 ? 'colspan="'.intval($vv['cs']).'"' : $colspan);
                $rowspan = (isset($vv['rs']) && intval($vv['rs'])>0 ? 'rowspan="'.intval($vv['rs']).'"' : $rowspan);
                if($vv['id_fld']>0){
                    $val = (is_array($fields) && array_key_exists($vv['id_fld'], $fields) ? $fields[$vv['id_fld']] : 0);
                    if($show){
                         $tbl .= '<td '.$colspan.' '.$rowspan.' >'.$val.'</td>';
                    }else{
                        $tbl .= '<td '.$colspan.' '.$rowspan.' ><input type="text" name="'.$vv['id_fld'].'" value="'.$val.'"></td>';
                    }
                }else{
                   # $tbl .= '<td '.$colspan.'>(r'.$vv['id_row'].')'.$vv['cap'].'</td>';
                    $tbl .= '<td '.$colspan.' '.$rowspan.' '.$center.'>'.$vv['cap'].'</td>';
                }
            }
            $tbl .= '</tr>';
        }
        $tbl .= '</table>'
                . '<input type="submit" name="addNalog"></form>';
        return $tbl;
    }
    
    public static function pre($data)
    {
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
    }
    public static function getFromSpr($idCat)
    {
        $db = Db::getConnection();
        $res = array();
        
        $sql = "SELECT * FROM esh_nalog_spr_val WHERE id_spr = :id";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $idCat, PDO::PARAM_INT);
        $result->execute();
        while($r = $result->fetch(PDO::FETCH_ASSOC)){
            $res[$r['id']] = $r['val'];
        }
        return $res;
    }
    public static function makeOptions($arr,$id){
        $res = '';
        if(is_array($arr)){
            foreach ($arr as $k=>$v){
                $sel = ($k==$id ? 'selected' : '');
                $res .= '<option value="'.$k.'" '.$sel.'>'.$v.'</option>';
            }
        }
        return $res;
    }
    public static function makeSelect($arr,$params)
    {
        $res = '<select name="'.$params['name'].'"><option value="0"></option>';
        $res .= self::makeOptions($arr,$params['selected']);
        $res .= '</select>';
        return $res;
    }
    public static function periodFormat($date)
    {
        return date('Y-m',strtotime($date)).'-01';
    }
}


