<?php 
$frmtitle = "Просмотр";

$rid = (isset($_SESSION['nalog']['rid']) ? $_SESSION['nalog']['rid'] : 0);
$rma = (isset($_SESSION['nalog']['rma']) ? $_SESSION['nalog']['rma'] : "");
$region = (isset($_SESSION['nalog']['region']) ? $_SESSION['nalog']['region'] : "");
$typeId = (isset($_SESSION['nalog']['type']) ? $_SESSION['nalog']['type'] : "");
$nalog_typeId = (isset($_SESSION['nalog']['nalog_type']) ? $_SESSION['nalog']['nalog_type'] : "");
$period = (isset($_SESSION['nalog']['period']) ? $_SESSION['nalog']['period'] : date('d.m.Y'));

$type = Form::getFromSpr(1);
$type = Form::makeSelect($type,array('name'=>'type','selected'=>$typeId));

$nalog_type = Form::getFromSpr(2);
$nalog_type = Form::makeSelect($nalog_type,array('name'=>'nalog_type','selected'=>$nalog_typeId));

$regionArr = Nalog::getRegionArr();
$sel = ($rid > 0 ? $rid : '');
$region = Form::makeSelect($regionArr,array('name'=>'rid','selected'=>$sel));
?>
<div class="form_container" style="width:550px">
	<div class="form_description">
		<h2><i class="fa fa-search-plus" aria-hidden="true"></i><?=$frmtitle;?></h2>
	</div>
	<form class="appnitro filter" method="post" action="php/handler.php">
            <table>
                <tr>
                    <td class="description">Минтакаи назорати андоз</td>
                    <td><?php echo $region; ?></td>
                </tr>
                <tr>
                    <td class="description">Давраи андоз</td>
                    <td><input type="date" name="period" value="<?php echo $period; ?>"></td>
                </tr>
                <tr>
                    <td class="description">Намуди андоз</td>
                    <td><?php echo $nalog_type; ?></td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
			<input type="submit" name="show" value="Просмотр">
                    </td>
                </tr>
            </table>
	</form>
</div>