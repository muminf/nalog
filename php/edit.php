<?php
$data = $_GET;
$data['period'] = Form::periodFormat($data['period']);
$nalogID = Nalog::checkPeriod($data['period'], $data['rid'],$data['ntype']);

$fields = Nalog::getNalogList($nalogID['id'],$data['period'],$data['ntype']);

$fields = (isset($data['type']) && $data['type'] == 3 ? null : $fields);
$show = (isset($data['show']) ? 1 : 0);
echo Form::printTable($data['ntype'], $params,$fields,$show);

