<?php
date_default_timezone_set('Asia/Dushanbe');
session_start();
define("ROOT", "../");
require_once("../components/Autoload.php");

if(isset($_POST['getRegion'])){ #ajax запрашивает регион по rma/rcb
    $res = Nalog::getRegion($_POST['getRegion']);
    $_SESSION['nalog']['region'] = $res['name'];
    $_SESSION['nalog']['rma'] = $_POST['getRegion'];
    echo json_encode($res);
    exit;
}

if(isset($_POST['rid'])){ # Если отправлена главная форма(страница new)
    $access = Nalog::checkAccessToRegion(1,1);
    if($access){
        $_SESSION['nalog']['rid'] = $_POST['rid'];
        $_SESSION['nalog']['rma'] = $_POST['rma'];
        $_SESSION['nalog']['nalog_type'] = $_POST['nalog_type'];
        $_SESSION['nalog']['type'] = $_POST['type'];
        $_SESSION['nalog']['period'] = $_POST['period'];
        $header = "&rid=".intval($_POST['rid']);
        $header .= (isset($_POST['type']) ? "&type=".$_POST['type'] : "");
        $header .= "&ntype=".$_POST['nalog_type']."&period=".$_POST['period']; 
        $header .= (isset($_POST['show']) ? "&show=1" : "");
        header("Location:../index.php?c=edit".$header);
    }
}

if(isset($_POST['addNalog'])){ # Если редактирована таблица налога
    $data = $_SESSION['nalog'];
    $data['period'] = Form::periodFormat($data['period']);
    unset($_POST['addNalog']);
    $summa = Nalog::getSummFromArr($_POST);
    $nalogID = Nalog::checkPeriod($data['period'], $data['rid'],$data['nalog_type']);
    
    $id_user = 1;
    $fields = serialize($_POST);
   
    if(!$nalogID){ #если не существует запись
        $nalogID = Nalog::addNalog($data['rid'], $data['period'], $data['nalog_type'], $summa);
        Nalog::addNalogList($nalogID, $_POST);
    }else{ #если существует запись
        if($data['type'] <> 3){ # != иловаги
            Nalog::editNalog($nalogID['id'], $summa);
            Nalog::editNalogList($nalogID['id'], $_POST);
            $data['type'] = 2;
        }else{ # = иловаги
            Nalog::plusToNalog($nalogID['id'], $summa);
            Nalog::plusToNalogList($nalogID['id'], $_POST);
        }
    }
    Nalog::addToLog($nalogID['id'], $id_user, date('Y-m-d H:i:s'), $data['type'], $fields);
}  