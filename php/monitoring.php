<?php 
$frmtitle = "Мониторинг";

$rid = (isset($_POST['rid']) ? $_POST['rid'] : 0);
$region = (isset($_POST['region']) ? $_POST['region'] : "");
$period = (isset($_POST['period']) ? $_POST['period'] : date('d.m.Y'));



$regionArr = Nalog::getRegionArr();
$region = Form::makeSelect($regionArr,array('name'=>'rid','selected'=>$rid));
?>
<div class="form_container" style="width:550px">
	<div class="form_description">
		<h2><i class="fa fa-list" aria-hidden="true"></i><?=$frmtitle;?></h2>
	</div>
	<form class="appnitro filter" enctype="multipart/form-data" method="post" action="index.php?c=monitoring">
            <table>
                <tr>
                    <td class="description">Минтакаи назорати андоз</td>
                    <td><?php echo $region; ?></td>
                </tr>
                <tr>
                    <td class="description">Давраи андоз</td>
                    <td><input type="date" name="period" value="<?php echo $period; ?>"></td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
			<input type="submit" name="monitoring" value="Просмотр">
                    </td>
                </tr>
            </table>
	</form>
</div>

<?php
echo Nalog::getMonitoring($period, $rid);
?>