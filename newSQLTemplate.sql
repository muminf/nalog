procedure rep(date1 date,date2 date, depart ref[DEPART]) is
	rBeg 	number;
	iRow 	number;	
	i		number;
	filial	varchar2;
	r33		number := 0;
	r37		number := 0;
	r61		number := 0;
	r72		number := 0;
	curDep	varchar2(20);
	rCount	number;
	dep		ref[DEPART];
	accName varchar2;
	curAcc	number;
begin
	excel.open_sheet(1);	
	rBeg := 5;
	iRow := rBeg + 1;
	curDep := '0';
	accName := '';
	curAcc := 0;
	for d in ::[AC_FIN] all
			WHERE (depart is null or d.depart.esh_depart_ref = depart.esh_depart_ref)
			and d.main_usv.num in('10101','10117')
			order by d.depart.code
	loop
		
		if curDep <> d.[DEPART].[CODE] then
			if substr(curDep,1,7) <> substr(d.[DEPART].[CODE],1,7) and curDep <> '0' then
				rCount := iRow - rBeg - 1;
				dep := ::[DEPART](code= substr(curDep,1,7));
				excel.write(rBeg,2,dep.name);
				excel.write(rBeg,3,'=SUM(R[+' || rCount || ']C:R[1]C)');
				excel.write(rBeg,4,'=SUM(R[+' || rCount || ']C:R[1]C)');
				excel.write(rBeg,5,'=SUM(R[+' || rCount || ']C:R[1]C)');
				excel.write(rBeg,6,'=SUM(R[+' || rCount || ']C:R[1]C)');
				iRow := iRow + 1;
				rBeg := iRow - 1;
			end if;
			curDep := d.[DEPART].[CODE];
			for ac in ::[AC_FIN] all
				WHERE ac.depart = d.depart
				and ac.main_usv.num in('10117','10101')	
				order by ac.main_usv.num
			loop
				if ac.main_usv.num = '10117' then
					select a(
							sum(a.arc_move.summa)
						) in ::[AC_FIN] all
						where a%id = ac%id --10117
						and a.arc_move.acc_corr.main_usv.num = '10111'
						and a.arc_move.date<=date2 + 1
						and a.arc_move.date>=date1
						and a.arc_move.dt = true
					into r61;
					select a(
							sum(a.arc_move.summa)
						) in ::[AC_FIN] all
						where a%id = ac%id --10117
						and a.arc_move.acc_corr.main_usv.num = '10111'
						and a.arc_move.date<=date2 + 1
						and a.arc_move.date>=date1
						and a.arc_move.dt = false
					into r33;
					accName := ac.name;
				else
					for(
						select a(
							a.arc_move.summa 		: c_sum
							,a.arc_move.acc_corr 	: c_corr
							,a.arc_move.dt			: c_dt
						) in ::[AC_FIN] all
						where a%id = ac%id --10101
						and a.arc_move.acc_corr.main_usv.num in ('22202','22802')
						and a.arc_move.date<=date2 + 1
						and a.arc_move.date>=date1
						order by a.arc_move.acc_corr
					)loop
						if curAcc <> a.c_corr%id then
							if r33>0 or r61>0 or r37>0 or r72>0 then
								excel.write(iRow,1,ac.[DEPART].[ESH_DEPART_REF].[CODE]);
								excel.write(iRow,2,accName);
								excel.write(iRow,3,r33);
								excel.write(iRow,4,r37);
								excel.write(iRow,5,r61);
								excel.write(iRow,6,r72);
								r33 = r61 = r37 = r72 := 0;
								accName := '';
								iRow := iRow + 1;
							end if;
							curAcc := a.c_corr%id;
							accName := a.c_corr.name;
						end if;
						if curAcc = 0 then
							curAcc := a.c_corr%id;
							accName := a.c_corr.name;
						end if;
						
						if a.c_dt = false then
							r72 := r72 + a.c_sum;
						else
							r37 := r37 + a.c_sum;
						end if;
					end loop;
				end if;
				if r33>0 or r61>0 or r37>0 or r72>0 then
					excel.write(iRow,1,ac.[DEPART].[ESH_DEPART_REF].[CODE]);
					excel.write(iRow,2,accName);
					excel.write(iRow,3,r33);
					excel.write(iRow,4,r37);
					excel.write(iRow,5,r61);
					excel.write(iRow,6,r72);
					r33 = r61 = r37 = r72 := 0;
					accName := '';
					iRow := iRow + 1;
				end if;
			end loop;
		end if;
	end loop;
	rCount := iRow - rBeg - 1;
	dep := ::[DEPART](code= substr(curDep,1,7));
	excel.write(rBeg,2,dep.name);
	excel.write(rBeg,3,'=SUM(R[+' || rCount || ']C:R[1]C)');
	excel.write(rBeg,4,'=SUM(R[+' || rCount || ']C:R[1]C)');
	excel.write(rBeg,5,'=SUM(R[+' || rCount || ']C:R[1]C)');
	excel.write(rBeg,6,'=SUM(R[+' || rCount || ']C:R[1]C)');
	excel.borders( 5, 1, iRow - 1, 7, 'RLVHTB');		
end;

procedure filial(date1 date,date2 date, depart ref[DEPART]) is
	iRow 	number;	
	i		number;
	fil ref[DEPART];
	depCode varchar2 := '0';
	dt	varchar2(15);
	kt	varchar2(15);
	type rec is record(
		dt	 varchar2(30)
		,kt	 varchar2(30)
		,sum  number
	);
	type tbl is table of rec;
	data tbl;
begin
	iRow := 5;
	excel.AddCopySheet('Лист1','Межфилиальные');
	excel.open_sheet('Межфилиальные');
	for dep in ::[DEPART] all where length(dep.[CODE]) = 7 and substr(dep.code,1,3) = '001'
		and (depart is null or dep.esh_depart_ref = depart.esh_depart_ref) order by dep.code loop
		i := 1;	
		for(
			select d(
						d.acc_dt		 		 : c_dt
					,d.acc_kt		    	 : c_kt
					,substr(d.nazn,1,3)		 : c_fil
					,d.sum					 : c_sum
					,d.DOCUMENT_NUM			 : c_num
					,d.nazn					 : c_nazn
				) in ::[MAIN_DOCUM] all
			WHERE (d.[ACC_DT]%id = 11873628231 or d.[ACC_KT]%id = 11873628231) --.main_v_id = '10111972100010100001'
			and trunc(d.[DATE_PROV])>=date1 and trunc(d.[DATE_PROV])<=date2
			and d.depart.esh_depart_ref = dep
		)loop
			depCode := d.c_fil;
			
			if d.c_dt%id = 11873628231 then
				dt := depCode;
				kt := substr(d.c_kt.depart.code,5,3);
			elsif d.c_kt%id = 11873628231 then
				dt := substr(d.c_dt.depart.code,5,3);
				kt := depCode;
			end if;
			
			data(i).dt := dt;
			data(i).kt := kt;
			data(i).sum := d.c_sum;
			i := i + 1;
		end loop;
		
			depCode := substr(dep.code,5,3);
			excel.merge(iRow,1,iRow,2);
			excel.set_font_bold(iRow,1,iRow,2);
			excel.allign(iRow,1,iRow,2,'L');
			excel.write(iRow,1,dep.name);
			iRow := iRow + 1;
			if data.count>0 then
				for i in data.first..data.last loop
					excel.allign(iRow,1,iRow,2,'R');
					begin
						if data(i).dt = depCode then
							excel.write(iRow,1,data(i).kt);
							fil := ::[DEPART](CODE=data(i).kt);
							excel.write(iRow,2,fil.name);
							excel.write(iRow,3,data(i).sum);
							iRow := iRow + 1;
						end if;
						if data(i).kt = depCode then
							excel.write(iRow,1,data(i).dt);
							fil := ::[DEPART](CODE=data(i).dt);
							excel.write(iRow,2,fil.name);
							excel.write(iRow,5,data(i).sum);
							iRow := iRow + 1;
						end if;
						exception when others then
						null;
					end;
				end loop;
			end if;
	end loop;
	excel.borders(5,1,iRow-1,6,'RLHVTB');
end;


procedure mhb(date1 date,date2 date, depart ref[DEPART]) is
	iRow 	number;	
	i		number;
	fil ref[DEPART];
	depCode varchar2 := '0';
	dt	varchar2(20);
	kt	varchar2(20);
	type rec is record(
		dt	 varchar2(30)
		,kt	 varchar2(30)
		,sum  number
	);
	type tbl is table of rec;
	data tbl;
begin
	iRow := 5;
	excel.AddCopySheet('Лист1','ЦБО');
	excel.open_sheet('ЦБО');
	for dep in ::[DEPART] all where length(dep.[CODE]) = 7 and substr(dep.code,1,3) = '001'
		and (depart is null or dep.esh_depart_ref = depart.esh_depart_ref) order by dep.code loop
		i := 1;	
		for(
			select d(
					d.acc_dt		 		 : c_dt
					,d.acc_kt		    	 : c_kt
					,substr(d.nazn,1,14)	 : c_nazn
					,substr(d.nazn,15)		 : c_dep
					,d.sum					 : c_sum
					,d.DOCUMENT_NUM			 : c_num
				) in ::[MAIN_DOCUM] all
			WHERE substr(d.nazn,1,14) = 'Таъмини хазина' --(d.[ACC_DT]%id = 11873628231 or d.[ACC_KT]%id = 11873628231) --.main_v_id = '10111972100010100001'
		--	and trunc(d.[DATE_PROV])>=date1 and trunc(d.[DATE_PROV])<=date2
			and d.depart.esh_depart_ref = dep
			and d%id = 12499989208
		)loop
			depCode := d.c_dep;
			
			if d.c_dt.main_usv.num = '10117' then
				dt := depCode;
				kt := d.c_kt.depart.code;
			elsif d.c_kt.main_usv.num = '10117' then
				dt := d.c_dt.depart.code;
				kt := depCode;
			end if;
			debug_pipe(dt || '    ' || kt || '    ' || d.c_sum || '    ' || d.c_nazn || '    ' || d.c_dep);
			/*
			data(i).dt := dt;
			data(i).kt := kt;
			data(i).sum := d.c_sum;
			i := i + 1;*/
		end loop;
		
		/*	depCode := substr(dep.code,5,3);
			excel.merge(iRow,1,iRow,2);
			excel.set_font_bold(iRow,1,iRow,2);
			excel.allign(iRow,1,iRow,2,'L');
			excel.write(iRow,1,dep.name);
			iRow := iRow + 1;
			if data.count>0 then
				for i in data.first..data.last loop
					excel.allign(iRow,1,iRow,2,'R');
					begin
						if data(i).dt = depCode then
							excel.write(iRow,1,data(i).kt);
							fil := ::[DEPART](CODE=data(i).kt);
							excel.write(iRow,2,fil.name);
							excel.write(iRow,3,data(i).sum);
							iRow := iRow + 1;
						end if;
						if data(i).kt = depCode then
							excel.write(iRow,1,data(i).dt);
							fil := ::[DEPART](CODE=data(i).dt);
							excel.write(iRow,2,fil.name);
							excel.write(iRow,5,data(i).sum);
							iRow := iRow + 1;
						end if;
						exception when others then
						null;
					end;
				end loop;
			end if;*/
	end loop;
	excel.borders(5,1,iRow-1,6,'RLHVTB');
end;