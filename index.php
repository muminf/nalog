<?php session_start(); ?>
<title>Налог</title>
<meta charset="utf-8">
<script src="js/jquery.min.js" type="text/javascript" charset="UTF-8" language="javascript"></script>
<script src="js/script.js" type="text/javascript" charset="UTF-8" language="javascript"></script>
<link rel="stylesheet" type="text/css" href="css/style.css" >
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<?php
define("ROOT", dirname(__FILE__));
require_once(ROOT."\components\Autoload.php");
$params = array('class'=>'GeneratedTable',
                'id'=>'',
                'width'=>1000,
                'align'=>'center'); 
?>
<a href="index.php?c=new"><h2>Добавление</h2></a>
<a href="index.php?c=show"><h2>Просмотр</h2></a>
<a href="index.php?c=monitoring"><h2>Мониторинг</h2></a>
<style>
    h2{
        margin-left:20px;
        display:inline;
    }
</style>
<?php
$pages = array('new','edit','show','monitoring');
if(isset($_GET['c']) && in_array($_GET['c'], $pages)){
    include('php/'.$_GET['c'].'.php');
}else{
    include('php/new.php');
}
?>



